import {Row, Col} from 'react-bootstrap';

export default function HomeBanner({bannerData}) {
    return(
        <div className='homePage'>
        <Row className='p-2'>
        <Col className='text-center mt-5'>
        <h1 className='bannerText'> {bannerData.title}</h1>
        <p className='my-3 bannerText1'>{bannerData.content}</p>
        <a className='btn bannerbutton text-white' href='/products'>SHOP NOW</a>  
        </Col>
        </Row>
        </div>
        );
    };
    
    