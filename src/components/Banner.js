import {Row, Col} from 'react-bootstrap';

export default function Banner({bannerData}) {
    return(
        <Row className='p-5'>
        <Col className='text-center'>
        <h1> {bannerData.title}</h1>
        <p className='my-3'>{bannerData.content}</p>
        <a className='btn btn-dark' href='/'>SHOP NOW</a>  
        </Col>
        </Row>
        );
    };
    
    