import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import {  faGaugeHigh,faUserEdit, faTag, faReceipt, faHandHoldingDollar, faPenToSquare, faSignOut } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function AppSideNavBar(){
  
  return(
    <Navbar  expand="lg" className='sidebar fixed-top' bg='white'>
    <Navbar.Toggle aria-label='test'/>
    <Navbar.Collapse>
    <Nav className='flex-column'>
    <Navbar.Brand>SAPATOS Admin Panel</Navbar.Brand>
    <Link to="/admin" className='nav-link border-bottom text-dark '><FontAwesomeIcon icon={faGaugeHigh}></FontAwesomeIcon> Main dashboard </Link>
    <Link to="/admin-view-products" className='nav-link border-bottom text-dark '><FontAwesomeIcon icon={faTag}></FontAwesomeIcon> Products</Link>
    <Link to="/admin-view-orders" className='nav-link border-bottom text-dark '><FontAwesomeIcon icon={faReceipt}></FontAwesomeIcon> Orders</Link>
    <Link to="/admin-view-customer" className='nav-link border-bottom text-dark '><FontAwesomeIcon icon={faUserEdit}></FontAwesomeIcon> Customer</Link>
    <Link to="#"  className='nav-link border-bottom text-dark'> <FontAwesomeIcon icon={faHandHoldingDollar}></FontAwesomeIcon>Sales</Link>
    <Link to="/change-password" className='nav-link border-bottom text-dark '><FontAwesomeIcon icon={faPenToSquare}></FontAwesomeIcon> Change Password</Link>
    <Link to="/logout" className='nav-link border-bottom text-dark '><FontAwesomeIcon icon={faSignOut}></FontAwesomeIcon> Logout</Link>
    </Nav>
    </Navbar.Collapse>
    </Navbar>
    );
  };
  
  
  