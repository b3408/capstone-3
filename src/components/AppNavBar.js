import { Navbar, Nav, Container, NavDropdown} from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext } from 'react';
import { Link } from 'react-router-dom';
import {  faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function AppNavBar (){

    const  { user } =  useContext(UserContext)  ;
   
    return(
    <Navbar bg="dark" expand="lg">
     <Container>
         <img src='Sapatos-logos_white.png' className='img-fluid LOGO'></img>
        <Navbar.Brand className='text-white'>
            Sapatos</Navbar.Brand>
        <Navbar.Toggle aria-label="basic-navbar-nav"/>
        <Navbar.Collapse>
            <Nav className='ml-auto'>
               <Link to="/" className='nav-link text-white'>Home</Link>
               <Link to="/products" className='nav-link text-white'>Products</Link>
               <NavDropdown  title={"Brands"}  id="nav-dropdown">
                    <NavDropdown.Item href="/products">Nike</NavDropdown.Item>
                    <NavDropdown.Item href="/products">Adidas</NavDropdown.Item>
                    <NavDropdown.Item href="/products">Onitsuka Tiger</NavDropdown.Item>
                    <NavDropdown.Item href="/products">Reebok</NavDropdown.Item>
                    <NavDropdown.Item href="/products">Sketches</NavDropdown.Item>
                    <NavDropdown.Item href="/products">New Balance</NavDropdown.Item>
                </NavDropdown>
               {user.id !== null ? 
               <>
                <NavDropdown  title={
                    <FontAwesomeIcon icon={faShoppingCart}></FontAwesomeIcon>      
                    } id="nav-dropdown">
                    <NavDropdown.Item href="/cart">My Cart</NavDropdown.Item>
                    <NavDropdown.Item href="/change-password">Change Password</NavDropdown.Item>
                    <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="/contact-us">Need Help?</NavDropdown.Item>
                </NavDropdown>  
                </>
               :
               <>
               <Link to="/login" className='nav-link text-white'>Login</Link>
               <Link to="/register" className='nav-link text-white'>Register</Link>
               </>
               }
            </Nav>   
        </Navbar.Collapse>
     </Container>
   
    </Navbar>
        );
    };
    export default AppNavBar;
    
    