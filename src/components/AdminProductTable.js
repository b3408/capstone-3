//identify needed components
import { Button, Card,  } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAdd, faEdit, faTrash, faBoxArchive, faCircle } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';
import Swal from 'sweetalert2';


export default function AdminProductCard({productProp}){
  const isActive = useState('');
  let id = productProp._id
  let token = localStorage.getItem('accessToken');
  const active = async () =>{
    fetch(`https://capstone2-batch156.herokuapp.com/product/${id}/archive`,{
      method: 'PUT',
      headers: {
				'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
			},
      body: JSON.stringify ({
        isActive: isActive
      })
    }).then(res => res.json()).then(convertedData => {
       if (convertedData) {
      return true
    
       } else {
         return false
       }
    })
    await Swal.fire({
      icon: 'success',
      title: 'Archived Successfully!',
      text: 'Please wait while redirecting.',
      timer:1500
    })  
     window.location.reload()
   
  }
  const notactive = async () =>{
   await fetch(`https://capstone2-batch156.herokuapp.com/product/${id}/unarchive`,{
      method: 'PUT',
      headers: {
				'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
			},
      body: JSON.stringify ({
        isActive: isActive
      })
    }).then(res => res.json()).then(convertedData => {
      if ( convertedData) {
        return true
      } else {
        return false
      }
    })
   await Swal.fire({
      icon: 'success',
      title: 'UnArchived Successfully!',
      text: 'Please wait while redirecting.',
      timer:2000
    })  
    window.location.reload()
  }

  const remove = async () =>{
    
   await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      width: '300px',
    }).then(async (result) =>   {
      if (result.isConfirmed) {
        fetch(`https://capstone2-batch156.herokuapp.com/product/${id}/delete`,{
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
          }
        }).then(res => res.json()).then(convertedData => {
          if (convertedData) {
      
          } else {
            return false
          }
         
        })
   await Swal.fire(
          'Deleted!',
          'Your file has been deleted.',
          'success',
          1500
        )
        window.location.href  ='/admin-view-products'
      }else{
        window.location.href  ='/admin-view-products'
      }
    })

  }




  return(
      <>
      <Card className='my-3' >
           <Row>
          <Card.Body className='d-flex justify-content-between AdminProductForm' > 
            <Col md={4}> 
            <Card.Img variant='top' src={`${productProp._id}.jpg`} className='mt-2 w-25' />
            </Col>
            <Col md={4}>
            <Card.Title>
                 {productProp.name}
              </Card.Title>
            </Col>
            <Col md={2}>
            <Card.Text>{productProp.description}</Card.Text>
            </Col>
            <Col md={2}>
            <Card.Text className='pl-2'>  {`Price:\u20B1${productProp.price}`} </Card.Text>
            </Col>
          </Card.Body>
       
          </Row>
          
      </Card>
      <div className='d-flex'>
     <Link to={`/add-product`} title='Add Product' className='btn btn-primary'> <FontAwesomeIcon icon={faAdd}/></Link>
          <Link to={`/edit-product/${productProp._id}`} title='Edit Product'className='ml-2 btn btn-primary '><FontAwesomeIcon icon={faEdit}/></Link>
          <Button className='ml-2 btn  bg-danger border-danger' onClick={remove} title='Delete Product'><FontAwesomeIcon icon={faTrash}/></Button>
            {
                productProp.isActive ? 
                <>
                   <Button  className='ml-2 btn  bg-warning border-warning' onClick={active} value={isActive} title='Archive Product' ><FontAwesomeIcon icon={faBoxArchive}/></Button>
                <h5 className='ml-5'> Active Status: <FontAwesomeIcon title='active' icon={faCircle} className='text-success'/></h5>
                </>
                :
                <>
                 <Button  className='ml-2 btn  bg-success border-warning' onClick={notactive} value={isActive} title='UnArchived Product' ><FontAwesomeIcon icon={faBoxArchive}/></Button>
                <h5 className=' ml-5'> Active Status: <FontAwesomeIcon icon={faCircle} className='text-danger' title='Not Active'/></h5>
                </>
            }
            </div>
      </>
      
  )  
};
