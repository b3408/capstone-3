import { Nav,  } from "react-bootstrap";
import { Navbar,Container } from "react-bootstrap";


export default function Footer () {
  return (
    <>
    <div>
    <Navbar bg="light" expand="lg" className="FooterForm" fixed="bottom">
    <Container>
    <Navbar.Brand href="#home">Sapatos</Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="me-auto">
    <Nav.Link href="#home">Home</Nav.Link>
    <Nav.Link href="#link">About Us</Nav.Link>
    <Nav.Link href="#link">Contact Us</Nav.Link>
    <Nav.Link href="#link">Browse Products</Nav.Link>
    
    </Nav>
    <Navbar.Text className="ml-5 text-dark">Design by: James David</Navbar.Text>
    </Navbar.Collapse>
    </Container>
    </Navbar>
    </div>
    </>
    )
  }