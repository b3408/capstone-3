import { Card, Container} from "react-bootstrap"
import map from "lodash/map"
import reduce from "lodash/reduce"

export default function CartCard (cartProp) {
    
    const productName = map(cartProp, 'productName')
    const price = map(cartProp, 'price')
    const qty = map(cartProp, 'quantity')
    const newPrice = qty * price
    
    return(
        <div>
        <Container className="d-flex justify-content-between " >
        <span >
        </span>
        <span className="w-25">{productName}</span>
        <span className="w-25">{qty}</span>
        <span className="w-25">*</span>
        <span className="w-25">{price}</span>
        <span className="w-25">{newPrice}</span>
        </Container>
        </div>
        )
        
    }