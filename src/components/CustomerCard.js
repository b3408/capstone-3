import { Table } from "react-bootstrap"
import map from "lodash/map"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faUser,  faUserNinja  } from "@fortawesome/free-solid-svg-icons";
import Swal from "sweetalert2";


export default function CustomerCard (userProp){
  
  let firstName = (map(userProp, 'firstName'));
  let email = (map(userProp, 'email'));
  let lastName = (map(userProp, 'lastName'));
  let userName = (map(userProp, 'userName'));
  let isAdmin = (map(userProp, 'isAdmin'));
  let id = (map(userProp, '_id'));
  const Role = JSON.parse(isAdmin)
  
  const remove = async () =>{
    const token = localStorage.getItem('accessToken')
    await Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      width: '300px',
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`https://capstone2-batch156.herokuapp.com/users/${id}/delete`,{
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      }).then(res => res.json()).then(convertedData => {
        if (convertedData) {
          return true
        } else {
          return false
        }
      })
    }
  })
  await Swal.fire(
    'Deleted!',
    'Your file has been deleted.',
    'success',
    1500
    )
    window.location.href ="/admin-view-customer"
  }
  
  
  return(
    
    <Table striped bordered hover variant="light" className="m-auto" >
    <tbody >
    <tr className="d-flex justify-content-between">
    <td  className="w-25">{firstName}</td>
    <td className="w-25">{lastName}</td>
    <td className="w-25">{email}</td>
    <td className="w-25">{userName}</td>
    {
      Role   ? 
      <td>  <button className="btn btn "><FontAwesomeIcon title="Admin" icon={faUserNinja}></FontAwesomeIcon></button></td>
      :
      <td>  <button className="btn btn "><FontAwesomeIcon  title="Admin"icon={faUser}></FontAwesomeIcon></button></td>
    }
    <td>  <button className="btn btn" onClick={remove}><FontAwesomeIcon icon={faTrash}></FontAwesomeIcon></button></td>
    </tr>
    </tbody>
    </Table>
    )
  }