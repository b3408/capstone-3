import {Row, Col, Card, Container,Button} from 'react-bootstrap'

//format the card with hte help of utility classes of the bootstrap

export default function Highlights(){
    return(
        <div className='highlightsForm'>
        
        <Container>
        <h1 className='pt-5 text-center text-white BRAND'>BRANDS</h1>
        <Row className=''>
            {/* 1st Highlights */}
            <Col xs={12} md={4} className='m-auto' >
                
            <Card className="p-4 cardHighlight1 text-center Nike">
                <Card.Body>
                    <Card.Title>Nike</Card.Title>
                    <Card.Text>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    </Card.Text>
                    <Button className='btn btn-dark' href="/products">VIEW COLLECTION</Button>
                </Card.Body>
            </Card>
            </Col>
             {/* 2nd Highlights */}
             <Col xs={12} md={4} className='m-auto'>
             <Card className="p-4 cardHighlight1 text-center Reebok">
                <Card.Body>
                    <Card.Title>Reebok</Card.Title>
                    <Card.Text>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    </Card.Text>
                    <Button className='btn btn-dark' href="/products">VIEW COLLECTION</Button>
                </Card.Body>
            </Card>
            </Col>
              {/* 3rd Highlights */}
              <Col xs={12} md={4} className='m-auto'>
              <Card className="p-4 cardHighlight1 text-center Adidas">
                <Card.Body>
                    <Card.Title>Adidas</Card.Title>
                    <Card.Text>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    </Card.Text>
                    <Button className='btn btn-dark'href="/products">VIEW COLLECTION</Button>
                </Card.Body>
            </Card>
            </Col>
        </Row>
        <Row className='my-3'>
            {/* 1st Highlights */}
            <Col xs={12} md={4} className='m-auto' >
                
            <Card className="p-4 cardHighlight1 text-center Newbalance">
                <Card.Body>
                    <Card.Title>New Balance</Card.Title>
                    <Card.Text>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    </Card.Text>
                    <Button className='btn btn-dark'href="/products">VIEW COLLECTION</Button>
                </Card.Body>
            </Card>
            </Col>
             {/* 2nd Highlights */}
             <Col xs={12} md={4} className='m-auto'>
             <Card className="p-4 cardHighlight1 text-center Sketchers">
                <Card.Body>
                    <Card.Title>Sketchers</Card.Title>
                    <Card.Text>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    </Card.Text>
                    <Button className='btn btn-dark' href="/products">VIEW COLLECTION</Button>
                </Card.Body>
            </Card>
            </Col>
              {/* 3rd Highlights */}
              <Col xs={12} md={4} className='m-auto'>
              <Card className="p-4 cardHighlight1 text-center Onitsukatiger">
                <Card.Body>
                    <Card.Title>Onitsuka Tiger</Card.Title>
                    <Card.Text>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
                    </Card.Text>
                    <Button className='btn btn-dark' href="/products">VIEW COLLECTION</Button>
                </Card.Body>
            </Card>
            </Col>
        </Row>
        </Container>
        </div>
    );
};

