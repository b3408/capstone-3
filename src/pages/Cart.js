import AppNavBar from "../components/AppNavBar";
import { useEffect, useState } from "react";
import { Navbar,Container,Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faArrowRight} from "@fortawesome/free-solid-svg-icons";
import CartCard from "../components/CartCard";
import UserContext from '../UserContext';
import { useContext } from 'react';

import { PayPalScriptProvider, PayPalButtons } from "@paypal/react-paypal-js";

export default function Cart(){
    let {user} = useContext(UserContext)
    let [total, setTotal] = useState(0);
    
    const [viewcart, setViewCart] = useState([])
    useEffect(()=>{
        let token = localStorage.getItem('accessToken');
        fetch('https://capstone2-batch156.herokuapp.com/users/getorders',{
        headers:{
            Authorization: `Bearer ${token}`
        }
    }).then(res => res.json())
    .then(convertedData => {
        let newConvertedData = convertedData.orders 
        
        setTotal(newConvertedData.reduce((prev, cur) => prev + cur.price* cur.quantity, 0));
        
        setViewCart(newConvertedData.map(products => {
            
            
            return(
                <CartCard key={products._id} cartProp={products}/>
                )
            }))
        });
        
    },[viewcart]);
    

        
        return(
            <>
            {user.isAdmin !== true ? 
                <>
                <AppNavBar/>
                <Navbar bg="light" expand="lg">
                <Container>
                <Navbar.Brand href="#home"><FontAwesomeIcon icon={faHome}></FontAwesomeIcon></Navbar.Brand>
                <Nav className="mr-auto ">
                <FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon>
                <h5 className="ml-3">Your Shopping Cart</h5>
                </Nav>
                </Container>
                </Navbar>
                <Container className="p-5 mb-5">
                <h1 className="text-center mt-3">Cart Items</h1>
                <div className="d-flex justify-content-between">
                <span className="w-25">Product</span>
                <span className="w-25">Qty</span>
                <span className="w-25">Price</span>
                <span className="w-25"></span>
                </div>
                <hr></hr>
                {viewcart}
                <hr></hr>
                <div className="d-flex justify-content-between">
                <span>Total Price:  </span>
                <span className="pl-5 w-25">{total}</span>
                </div>
                <div className="d-flex mt-2 justify-content-end pr-5 mr-5">
                    <h3 className="m-1">Checkout:</h3>
                <PayPalScriptProvider options={{ "client-id": "test" }}>
                    <PayPalButtons
                        createOrder={(data, actions) => {
                            return actions.order.create({
                                purchase_units: [
                                    {
                                        amount: {
                                            value: "1.99",
                                        },
                                    },
                                ],
                            });
                        }}
                        onApprove={(data, actions) => {
                            return actions.order.capture().then((details) => {
                                const name = details.payer.name.given_name;
                                alert(`Transaction completed by ${name}`);
                            });
                        }}
                    />
                </PayPalScriptProvider>
                </div>
                </Container></>
                :
                window.location.href ="/admin"
            }
            
            </>
            )
        };