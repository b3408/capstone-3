import Banner from './../components/Banner'
import AppNavBar from '../components/AppNavBar';


const data = {
    title:"404 Page Not Found",
    content:"The page you are looking for does not exist"
}

export default  function ErrorPage(){
    return(
        <div>
        <AppNavBar/>
        <Banner bannerData={data}/>
        </div>
        );
    };