import AppNavBar from '../components/AppNavBar';
import {useEffect, useState} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import swal from 'sweetalert2'
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';


export default function Login(){
    

    const [password, setPassword] = useState('');
    const [userName, setUserName] = useState('');
    const {user, setUser} = useContext(UserContext)
    
   
    const [isActive, setIsActive] = useState(false);
    
    useEffect(()=> {
        if (userName !== '' && password !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    },[ password, userName,user])
    
    const loginUser = async (eventSubmit) => {
        eventSubmit.preventDefault();
        await  fetch('https://capstone2-batch156.herokuapp.com/users/login/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify({
            userName: userName,
            password: password
        })
    })
    .then(res => res.json())
    .then(async convertedData => {
        let token = convertedData.access;
        if (typeof token !== 'undefined') {
            
            localStorage.setItem('accessToken',token)
            await fetch('https://capstone2-batch156.herokuapp.com/users/details',{
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(async convertedData =>{
            if (convertedData.isAdmin !== true) {
                setUser({
                    id: convertedData._id,
                    isAdmin: convertedData.isAdmin
                })
                await swal.fire({
                    icon: 'success',
                    title: 'Login Successfully!',
                    text: 'Please wait while redirecting.',
                    timer:2000
                })  
                window.location.href = "/products";
            }else if(convertedData.isAdmin === true){
                setUser({
                    id: convertedData._id,
                    isAdmin: convertedData.isAdmin
                })
                await  swal.fire({
                    icon: 'success',
                    title: 'Login as Admin Successfully!',
                    text: 'Please wait while redirect you into the Admin Dashboard.',
                    timer:2000
                })  
                window.location.href = "/admin";
            }
            else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })
    }
    else {
        swal.fire({
            icon: 'error',
            title: 'Check your Credentials!',
            text: 'Contact Admin if problem persist',
        })  
    }
});
};
return(
    <div>
    <AppNavBar/>
    <Container className='mt-5'>
    <h1 className='text-center'>Login</h1>
    <div className='border container mt-2' id='formBox'>
    <Form onSubmit={event => loginUser(event)} >
    {/* Username Field */}
    <Form.Group className='mt-5'>
    <Form.Label>Username</Form.Label>
    <Form.Control className='formLogin' type='text' required placeholder='Enter your Username' value={userName} onChange={event => {setUserName(event.target.value)}}></Form.Control>
    
    </Form.Group>
    {/* Password Field */}
    <Form.Group className='mt-2'>
    <Form.Label>Password</Form.Label>
    <Form.Control className='formLogin' type='password' required placeholder='Enter your password' value={password} onChange={event => {setPassword(event.target.value)}}></Form.Control>
    </Form.Group>
    {
        isActive ?
        <Button className='btn btn-block formLogin btn-primary ' type='submit'>Login</Button>
        :
        <Button className='btn btn-block btn-secondary' type='submit' disabled>Login</Button>
    }
    </Form>
    <p className='mt-3 '>Not registered yet?<Link to="/register">
    Click to Register
    </Link></p>
    </div>
    </Container>
    </div>
    );
}
