
import AppNavBar from '../components/AppNavBar';
import {Row, Card, Col, Button,Container} from 'react-bootstrap'
import { useParams } from 'react-router-dom';
import swal from 'sweetalert2'
import { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCartShopping, faHeart } from "@fortawesome/free-solid-svg-icons";
import ReactStars from "react-rating-stars-component";
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useContext } from 'react';
import { Form } from 'react-bootstrap';


export default function ProductView(){
    let {user} = useContext(UserContext)
    const [productInfo, setProductInfo] = useState({
        name: null,
        description: null,
        price: null
    });
    const [quantity, setQuantity] = useState('');
    
    const ratingChanged = async (newRating) => {
        if (newRating) {
            
            const { value: text } = await Swal.fire({
                input: 'textarea',
                inputLabel: 'Message',
                inputPlaceholder: 'Type your message here...',
                inputAttributes: {
                    'aria-label': 'Type your message here'
                },
                showCancelButton: true
            })
            
            if (text) {
                Swal.fire({
                    icon: 'success',
                    title: 'Thank you',
                    text:  'Thank you for the feedback!',
                    timer: 1500
                })
            }
        }
    };
    const {id} = useParams()
    
    useEffect(() =>{
        fetch(`https://capstone2-batch156.herokuapp.com/product/${id}`)
        .then(res => res.json())
        .then(convertedData => {
            setProductInfo({
                id: convertedData._id,
                brand: convertedData.brand,
                name: convertedData.name,
                description: convertedData.description,
                price: convertedData.price
            })
        });
    },[id])
    const addCart = async () =>{
      let token =  localStorage.getItem('accessToken')

        
        fetch('https://capstone2-batch156.herokuapp.com/users/order', {
                method:'POST',
                headers:{
                    'Authorization' : `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    productId: id,
                    quantity: quantity
                })
            }).then(res => res.json()).then(async convertedData =>{
               await  swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Added to Cart Successfully!',
                    showConfirmButton: false,
                    timer: 1500
                })  
                window.location.href= "/cart"
            })
          
        };
        
        const heart = () =>{
            return(
                swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Add to Favourite!',
                    showConfirmButton: false,
                    timer: 1500
                }) 
                );
            };
        return(
            <div>
            
            {
                user.isAdmin !== true ? 
                <>
                <AppNavBar/>
                <Row className='mt-5 ProductViewForm'>
                <Col md={{span:4, offset:2}} sm={12}>
                <Card.Img variant='top' src={`/${productInfo.id}.jpg`} className='m-5 w-75'/>
                </Col>
                <Col md={4} sm={12}>
                <Container>
                <Card >
                <Card.Body >
                {/* Course Name */}
                <Card.Title>
                <h2>{productInfo.name}</h2>
                </Card.Title>
                <Card.Title>
                <h3>Brand: {productInfo.brand}</h3>
                </Card.Title>
                <Card.Text>
                {`\u20B1${productInfo.price}`}
                </Card.Text>
                <Card.Text>
                Sizes
                </Card.Text>
                <Container className='d-flex justify-content-between'>
                <Row>
                
                <Col md={12} className="d-flex justify-content-between">
                <Button className="btn btn-dark">6</Button>
                <Button className="btn btn-dark">6.5</Button>
                <Button className="btn btn-dark">7</Button>
                <Button className="btn btn-dark">7.5</Button>
                <Button className="btn btn-dark">8.5</Button>
                
                </Col>
                <Col md={12} className="d-flex justify-content-between mt-2 mb-3">
                <Button className="btn btn-dark">9</Button>
                <Button className="btn btn-dark">9.5</Button>
                <Button className="btn btn-dark">10</Button>
                <Button className="btn btn-dark">10.5</Button>
                <Button className="btn btn-dark">11</Button>
                </Col>
                </Row>
                </Container>
                <Form className='mb-4'>
                <Form.Label>Quantity</Form.Label>
                <Form.Control type='number' value={quantity} onChange={event =>{setQuantity(event.target.value)}} required/>
                </Form>
                {/* Course Description */}
                <Card.Text>
                {productInfo.description}
                </Card.Text>
                <Card.Subtitle>
                Ratings
                </Card.Subtitle>
                <ReactStars
                count={5}
                onChange={ratingChanged}
                size={24}
                activeColor="#ffd700"
                />
                </Card.Body> 
                <div className='d-flex justify-content-center'>
                    {
                        
                        user.id !== null ? 
                        <Button className='bg-dark AddCart border-0 mb-2 mr-2' onClick={addCart}>
                        Add to Cart
                        <FontAwesomeIcon icon={faCartShopping} className='ml-2'></FontAwesomeIcon>
                        </Button>
                        :
                        <Button className='bg-dark AddCart border-0 mb-2 mr-2' href='/login'>
                        Login to buy
                        <FontAwesomeIcon icon={faCartShopping} className='ml-2'></FontAwesomeIcon>
                        </Button>
                    }
              
                <Button className='bg-dark AddCart border-0 mb-2' onClick={heart}>
                Add Favourite
                <FontAwesomeIcon icon={faHeart} className='ml-2'></FontAwesomeIcon>
                </Button>
                </div>
                
                </Card>
                </Container>
                </Col>
                
                
                </Row>
                </>
                :
                window.location.href ="/admin"
            }
            
            </div>
            )
        };