import { useEffect, useState } from 'react';
import AppSideNavBar from '../components/AppSideNavBar';
import AdminProductTable from '../components/AdminProductTable';
import { Container } from 'react-bootstrap';

export default function Products(){
    
    const [productCollection, setProductCollection] = useState([]);
    useEffect(()=> {
        fetch('https://capstone2-batch156.herokuapp.com/product/allproducts').then(res => res.json()).then(convertedData => {
        setProductCollection(convertedData.map(products => {
            return(
                <AdminProductTable key={products._id}  productProp={products} />
                )
            })) 
        });
    },[productCollection]);
    
    return(
        <div>
        <>
        <AppSideNavBar/>   
        <Container className='mb-5 pb-2 bg-light'>
        <h1 className='text-center mt-5'>PRODUCTS INFORMATION</h1>
        {productCollection}
        </Container>
        </>
        </div>
        );
    }
    