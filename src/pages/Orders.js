import OrdersCard from "../components/OrdersCard";
import AppSideNavBar from "../components/AppSideNavBar";

export default function Orders(){
    return(
        <>
          <AppSideNavBar/>
        <OrdersCard/>
        </>
      
    )
}