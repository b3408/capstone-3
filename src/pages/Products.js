import { useEffect, useState } from 'react';
import AppNavBar from '../components/AppNavBar';
import Banner from '../components/Banner';
import ProductCard from '../components/ProductCard';
import { Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext } from 'react';




const data = {
    title:'OPENING PROMO UP to 40% OFF',
    content:'Browse through our Products'
}



export default function Products(){
    let {user} = useContext(UserContext)
    const [productCollection, setProductCollection] = useState([]);
    useEffect(()=> {
        fetch('https://capstone2-batch156.herokuapp.com/product/activeproducts').then(res => res.json()).then(convertedData => {
            setProductCollection(convertedData.map(products => {
          
                return(
                    <ProductCard key={products._id}  productProp={products} />
                )
            })) 
        });
    },[]);
   
    return(

        <div className='mb-5'>
                {
                user.isAdmin !== true ?
                <>
                <AppNavBar/>   
                <div className='PromoSale'>
                <Banner bannerData={data}/>
                </div>
                <Container>
                {productCollection}
                </Container>
                </> 
                :
                window.location.href ="/admin"
                }
        </div>
    );
}
