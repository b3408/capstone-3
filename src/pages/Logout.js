import { useContext, useEffect } from 'react';
import UserContext from '../UserContext'
import Swal from 'sweetalert2';



export default function Logout(){
    const {setUser, unsetUser } = useContext(UserContext);
    const {user} = useContext(UserContext)

    useEffect(()=>{
    
      Swal.fire({
        title: 'Are you sure you want to logout? ',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Logout'
      
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Logout!',
            'You have been logout successfully',
            'success'
          )
          unsetUser();
          setUser({
            id: null,
            isAdmin: null
          })
          window.location.href ='/'
        }else{
          user.isAdmin === true ? 
          window.location.href ='/admin'
          :
          window.location.href ='/products'
        }
       
      })
    },[setUser, unsetUser, user])
   return (
     <div></div>
   )
}