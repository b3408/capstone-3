import './App.css';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Products from './pages/Products.js';
import Error from './pages/Error.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import ProductView from './pages/ProductView';
import UpdateProduct from './pages/UpdateProduct';
import AdminHome from './pages/AdminHome';
import ContactUs from './pages/ContactUs';
import Cart  from './pages/Cart';
import {BrowserRouter as Router, Routes, Route}from 'react-router-dom'
import { useState, useEffect } from 'react';
import {UserProvider} from './UserContext.js'
import AdminProducts from './pages/AdminProducts';
import Orders from './pages/Orders'
import Customer from './pages/Customer';
import ChangePassword from './pages/ChangePassword'
import Sales from './pages/Sales'
import EditProduct from './pages/EditProduct';
import AddProduct from './pages/AddProduct';
import Footer from './components/Footer'



  function App() {
    const [user, setUser] = useState({
      id:null,
      isAdmin: null
    });
    
    const unsetUser = () =>{
      localStorage.clear();
      setUser({
        id:null,
        isAdmin: null
      })
    };

    useEffect(() =>{
     let token = localStorage.getItem('accessToken');
     fetch('https://capstone2-batch156.herokuapp.com/users/details',{
      headers:{
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData =>{
     if (typeof convertedData._id !== 'undefined') {
        setUser({
          id: convertedData._id,
          isAdmin: convertedData.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
     },[user]);
  


  return (
    <div>
      <UserProvider value={{user, setUser, unsetUser}}>
   
      <Router>
       
          <Routes>
              <Route path='/' element={<Home/>}/>
              <Route path='/register' element={<Register/>} />
              <Route path='/login' element={<Login/>}/>
              <Route path='/products' element={<Products/>}/>   
              <Route path='/product/view-product/:id' element={<ProductView/>}/>
              <Route path='/logout' element={<Logout/>}/>
              <Route path='/products/update-product' element={<UpdateProduct/>}/>
              <Route path='*' element={<Error/>}/>
              <Route path='/admin' element={<AdminHome/>}/>
              <Route path='/contact-us' element={<ContactUs/>}/>
              <Route path='/cart' element={<Cart/>}/>
              <Route path='/admin-view-products' element={<AdminProducts/>}/>
              <Route path='/admin-view-orders' element={<Orders/>}/>
              <Route path='/admin-view-customer' element={<Customer/>}/>
              <Route path='/change-password' element={<ChangePassword/>}/>
              <Route path='/admin-sales' element={<Sales/>}/>
              <Route path='/edit-product/:id' element={<EditProduct/>}/>
              <Route path='/add-product' element={<AddProduct/>}/>
        
          </Routes>
          <Footer/>
      </Router>
   
      </UserProvider>
    </div>
  );
}

export default App;
